package e_takesh.android.e_takeshtaxi.helper;

import android.provider.BaseColumns;

/**
 * Created by tagakousoh on 08/01/2018.
 */

public class WaitlistContract {

    public static final class BasePrestataire implements BaseColumns {
        public static final String TABLE_NAME ="prestataire";
        public static final String COLUMN_PREST_ID ="PrestataireId";
    }
}

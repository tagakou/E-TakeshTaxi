package e_takesh.android.e_takeshtaxi.helper;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by tagakousoh on 08/01/2018.
 */

public class WaitlistDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME ="e-takesh.db";
    private static final int DATABASE_VERSION = 1;
    public WaitlistDbHelper(Context context){

        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
     public void onCreate(SQLiteDatabase sqLiteDatabase){
        final String SQL_CREATE_PRESTATAIRE_TABLE = "CREATE TABLE " +
                WaitlistContract.BasePrestataire.TABLE_NAME +"("+
                WaitlistContract.BasePrestataire._ID +" INTEGER PRIMARY KEY AUTOINCREMENT," +
                WaitlistContract.BasePrestataire.COLUMN_PREST_ID +" TEXT NOT NULL" +

                ")";

        sqLiteDatabase.execSQL(SQL_CREATE_PRESTATAIRE_TABLE);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1){
        sqLiteDatabase.execSQL("DROP_TABLE_IF_EXIST" + WaitlistContract.BasePrestataire.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

}

package e_takesh.android.e_takeshtaxi.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import e_takesh.android.e_takeshtaxi.R;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback{
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"onMap: reading");
        mMap=googleMap;


    }
    private static final String TAG= "MapActivity";
    private GoogleMap mMap;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_Permission_Request_CODE = 1234;
     Boolean mLocationPermissionGranted = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getLocationPermission();

    }
    public void InitMap(){
        Toast.makeText(this, "Init good", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"InitMap: reading");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

         mapFragment.getMapAsync(MapsActivity.this);


    }

        public  void getLocationPermission(){
            Toast.makeText(this, "Location good", Toast.LENGTH_SHORT).show();
            Log.d(TAG,"locationgood: reading");
            String[] permissions = {FINE_LOCATION,COARSE_LOCATION};
              if(ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                      if(ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                          mLocationPermissionGranted = true;
                      }else {
                          ActivityCompat.requestPermissions(this,permissions,LOCATION_Permission_Request_CODE );
                      }

            }else {
                  ActivityCompat.requestPermissions(this,permissions,LOCATION_Permission_Request_CODE );
              }
        }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Toast.makeText(this, "onRequestPermet", Toast.LENGTH_SHORT).show();
        Log.d(TAG,"onRequestpermet: reading");
        mLocationPermissionGranted = false;
        switch (requestCode){
            case LOCATION_Permission_Request_CODE :{
                if(grantResults.length > 0 ){
                    for(int i =0 ; i<grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show();
                            Log.d(TAG,"onfailedgrand: reading");
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                    Toast.makeText(this, "granted", Toast.LENGTH_SHORT).show();
                    Log.d(TAG,"ongranedsucces: reading");
                    mLocationPermissionGranted = true;
                    InitMap();
                }
            }

        }
    }






}

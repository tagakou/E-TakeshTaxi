package e_takesh.android.e_takeshtaxi.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import e_takesh.android.e_takeshtaxi.R;
import e_takesh.android.e_takeshtaxi.adapter.PresentationPagerAdapter;
import e_takesh.android.e_takeshtaxi.fragment.FragmentPresentation1;
import e_takesh.android.e_takeshtaxi.fragment.FragmentPresentation2;
import e_takesh.android.e_takeshtaxi.fragment.FragmentPresentation3;

/**
 * Created by tagakousoh on 01/06/2018.
 */

public class PresentationActivity extends AppCompatActivity {
    private final String TAG = "PresentationActivity";
    private PresentationPagerAdapter msectionStatesPagerAdapter;
    private ViewPager mviewpager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        msectionStatesPagerAdapter = new PresentationPagerAdapter(getSupportFragmentManager());
        mviewpager = (ViewPager) findViewById(R.id.containter);
        setupViewPager(mviewpager);
    }

    private void setupViewPager(ViewPager viewPager) {
        PresentationPagerAdapter adapter = new PresentationPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new FragmentPresentation1(), "FragmentPresentation1");
        adapter.addFragment(new FragmentPresentation2(), "FragmentPresentation2");
        adapter.addFragment(new FragmentPresentation3(), "FragmentPresentation3");
        viewPager.setAdapter(adapter);
    }

    //  fonction qui recupère le fragment courant et le charge dans l'activité

    public void setViewPager(int fragmentNumber) {
        mviewpager.setCurrentItem(fragmentNumber);

    }
}

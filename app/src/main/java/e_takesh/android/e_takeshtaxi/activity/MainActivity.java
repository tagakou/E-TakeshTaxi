package e_takesh.android.e_takeshtaxi.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import e_takesh.android.e_takeshtaxi.R;
import e_takesh.android.e_takeshtaxi.helper.GPStracker;
import e_takesh.android.e_takeshtaxi.service.MyServices;

public class MainActivity extends AppCompatActivity {
    Button btnGetLoc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent bindIntent = new Intent(getBaseContext(), MyServices.class);
        startService(bindIntent);

        btnGetLoc = (Button) findViewById(R.id.btnGetLoc);
        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},123);
        btnGetLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GPStracker g = new GPStracker(getApplicationContext());
                Location l = g.getLocation();
                if(l!=null){
                    double lat=l.getAltitude();
                    double lon=l.getLongitude();

                    Position(lat,lon);

                }
            }
        });
    }
    public void Position(double a, double b){
        AlertDialog.Builder ad=new AlertDialog.Builder(this);
        ad.setTitle("Voici tes coordonnées");
        ad.setMessage("Latitude = "+a+"\n"+
                "Longitude = "+b);
        ad.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ad.show();
    }

}


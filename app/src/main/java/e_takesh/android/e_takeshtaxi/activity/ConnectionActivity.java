package e_takesh.android.e_takeshtaxi.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import e_takesh.android.e_takeshtaxi.R;
import e_takesh.android.e_takeshtaxi.Utils.ApiUtils;
import e_takesh.android.e_takeshtaxi.helper.TestUtil;
import e_takesh.android.e_takeshtaxi.helper.WaitlistContract;
import e_takesh.android.e_takeshtaxi.helper.WaitlistDbHelper;
import e_takesh.android.e_takeshtaxi.model.Prestataire;
import e_takesh.android.e_takeshtaxi.remote.ApiClientService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static e_takesh.android.e_takeshtaxi.helper.WaitlistContract.BasePrestataire.TABLE_NAME;

public class ConnectionActivity extends AppCompatActivity {

    private final static String TAG = "ConnectionActivity";
    ApiClientService mAPIService;
    EditText mEditTextLogin, mEditTextMot_de_passe;
    Button Btnconnection;
    String Login,Mot_de_passe;
    private SQLiteDatabase mDb;
    private WaitlistDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        InitConnection();
        dbHelper = new WaitlistDbHelper(this);
        mDb = dbHelper.getWritableDatabase();
        TestUtil.insertFakeData(mDb);
       // addNewGuest("2");
        Log.e(TAG,"oui oui");
        mAPIService = ApiUtils.getAPIService();
        Btnconnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login = mEditTextLogin.getText().toString();
                Mot_de_passe =mEditTextMot_de_passe.getText().toString();
                if(validateConnection(Login,Mot_de_passe)){
                    activeAccount(Login,Mot_de_passe);

                }
            }
        });
    }


    private void InitConnection(){

        Btnconnection=(Button)findViewById(R.id.btnConnection);
        mEditTextLogin = (EditText) findViewById(R.id.EditTextLogin);
        mEditTextMot_de_passe = (EditText) findViewById(R.id.EditTextMot_de_passe);

    }

    private boolean validateConnection(String login, String mot_de_passe) {

        if (  login== null || login.trim().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.veillez_login) , Toast.LENGTH_LONG).show();

            return false;
        }
        if ( mot_de_passe == null || mot_de_passe.trim().length() == 0) {
            Toast.makeText(this, getResources().getString(R.string.veillez_mot_de_passe), Toast.LENGTH_LONG).show();

            return false;
        }



        return true;

    }

    private void activeAccount(final String username , final String  password) {

        String base = username + ":" + password;

        final String authHeader = Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);
        //Log.e(TAG, " authHeader"+ authHeader);
        //Toast.makeText( ConnectionActivity.this, ""+authHeader, Toast.LENGTH_SHORT).show();
        Call<Prestataire> call = mAPIService.connecterPrestataire(authHeader);

        call.enqueue(new Callback<Prestataire>() {

            @Override
            public void onResponse(Call<Prestataire> call, Response<Prestataire> response) {
                if (response.isSuccessful()) {
                    Toast.makeText( ConnectionActivity.this, "Good pass", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Good pass");
                    Log.e(TAG, " authHeader"+ authHeader);
                    Prestataire a=response.body();
                    addNewGuest(String.valueOf(a.getId()));
                    Intent intent=new Intent(ConnectionActivity.this,MainActivity.class);
                    startActivity(intent);
                    //Toast.makeText(ActivationCompteActivity.this, "" + a.getNom(), Toast.LENGTH_LONG).show();

                }else{
                    String error = null;
                    try {
                        error = response.errorBody().string();
                        JSONObject jsonObjectError = new JSONObject(error);
                        String description = jsonObjectError.getString("description");
                        Toast.makeText(ConnectionActivity.this, ""+description, Toast.LENGTH_SHORT).show();
                        //String errorDetails = jsonObjectError.getString("errorDetails");
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.e(TAG, "TRY"+ response.code());
                    Log.e(TAG, "TRY"+ response.message());


                }


                }


            @Override
            public void onFailure(Call<Prestataire> call, Throwable t) {


                if(call.isCanceled()) {
                    Toast.makeText( ConnectionActivity.this, "request was aborted", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "request was aborted");
                }else {
                    Toast.makeText( ConnectionActivity.this, "Unable to submit post to API", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Unable to submit post to API.");
                    Log.e(TAG, "doLogin onFaillure: " + t.getMessage());
                }

                Log.e(TAG, "Unable to submit post to API.");


            }

        });


    }

    private long addNewGuest(String id) {
        ContentValues cv = new ContentValues();
        cv.put(WaitlistContract.BasePrestataire.COLUMN_PREST_ID, id);

        return mDb.insert(TABLE_NAME, null, cv);
    }


    private Boolean removeGuest(Integer id) {

        return mDb.delete(TABLE_NAME,
                WaitlistContract.BasePrestataire._ID + "=" + id, null) > 0;

    }


}

package e_takesh.android.e_takeshtaxi.model;

/**
 * Created by tagakousoh on 09/05/2018.
 */

public class Service {

    private long id;
    private String intitule;
    private String duree;
    private String prix;


    public Service(String intitule, String duree, String prix) {
        this.intitule = intitule;
        this.duree = duree;
        this.prix = prix;
    }

    public String getIntitule() {
        return intitule;
    }

    public String getDuree() {
        return duree;
    }

    public String getPrix() {
        return prix;
    }

    public long getId() {
        return id;
    }
}

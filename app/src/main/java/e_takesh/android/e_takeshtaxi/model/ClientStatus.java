package e_takesh.android.e_takeshtaxi.model;

/**
 * Created by tagakousoh on 26/04/2018.
 */

public class ClientStatus {

    private String code;
    private String message;
    private String entity;
    private String field;
    private Integer id;



    public ClientStatus(String code, String message, String entity, String field, Integer id) {
        this.code = code;
        this.message = message;
        this.entity = entity;
        this.field = field;
        this.id = id;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }




}

package e_takesh.android.e_takeshtaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by tagakousoh on 09/04/2018.
 */

public class Prestataire {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("nom")
    @Expose
    private String nom;
    @SerializedName("prenom")
    @Expose
    private String prenom;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    private String photo;
    @SerializedName("date_naissance")
    @Expose
    private String date_naissance;
    @SerializedName("position")
    @Expose
    private Position position;
    private ArrayList<Prestation> prestation;


    public Prestataire(Long id, String nom, String prenom, String telephone, String date_naissance) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.date_naissance = date_naissance;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public String getPhoto() {
        return photo;
    }
    public Position getPosition() {
        return position;
    }

    public ArrayList<Prestation> getPrestation() {
        return prestation;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Long getId() {
        return id;
    }
}

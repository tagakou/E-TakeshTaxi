package e_takesh.android.e_takeshtaxi.model;

/**
 * Created by tagakousoh on 09/05/2018.
 */

public class Vehicule {

    private String immatriculation;
    private String couleur;
    private String marque;
    private Integer volume;
    private Integer nb_place;
    private categorieVehicule categorie;
    private Prestataire prestataire;

    public Vehicule(String immatriculation, String marque) {
        this.immatriculation = immatriculation;
        this.marque = marque;
    }
    
    public String getImmatriculation() {
        return immatriculation;
    }


    public String getCouleur() {
        return couleur;
    }


    public String getMarque() {
        return marque;
    }


    public Integer getVolume() {
        return volume;
    }

    public Prestataire getPrestataire() {
        return prestataire;
    }

    public Integer getNb_place() {
        return nb_place;
    }

    public categorieVehicule getCategorieVehicule() {
        return categorie;
    }
}

package e_takesh.android.e_takeshtaxi.model;

/**
 * Created by tagakousoh on 09/05/2018.
 */

public class Prestation {

    private String date;
    private String status;
    public  Prestataire prestataire;
    private  Vehicule vehicule;
    private  Service service;



    public Prestataire getPrestataire() {
        return prestataire;
    }

    public Vehicule getVehicule() {
        return vehicule;
    }

    public Service getService() {
        return service;
    }

    public String getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }
}

package e_takesh.android.e_takeshtaxi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tagakousoh on 19/02/2018.
 */

public class PresentationPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    public PresentationPagerAdapter(FragmentManager fm) {

        super(fm);
    }
    public void addFragment(Fragment fragment, String Title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(Title);
    }

    @Override
    public CharSequence getPageTitle(int Position){
        return mFragmentTitleList.get(Position);
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}

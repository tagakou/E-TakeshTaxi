package e_takesh.android.e_takeshtaxi.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import e_takesh.android.e_takeshtaxi.Utils.ApiUtils;
import e_takesh.android.e_takeshtaxi.activity.ConnectionActivity;
import e_takesh.android.e_takeshtaxi.helper.GPStracker;
import e_takesh.android.e_takeshtaxi.helper.TestUtil;
import e_takesh.android.e_takeshtaxi.helper.WaitlistContract;
import e_takesh.android.e_takeshtaxi.helper.WaitlistDbHelper;
import e_takesh.android.e_takeshtaxi.model.Client;
import e_takesh.android.e_takeshtaxi.model.Position;
import e_takesh.android.e_takeshtaxi.model.Prestataire;
import e_takesh.android.e_takeshtaxi.remote.ApiClientService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static e_takesh.android.e_takeshtaxi.helper.WaitlistContract.BasePrestataire.COLUMN_PREST_ID;
import static e_takesh.android.e_takeshtaxi.helper.WaitlistContract.BasePrestataire.TABLE_NAME;

/**
 * Created by tagakousoh on 21/02/2018.
 */

public class MyServices extends Service {
    public static final  String TAG = "MyServiceEventType";
    Boolean started=false;
    TimerTask task;
    Timer timer;
    double lat1;
    double lon1;
    ApiClientService mAPIService;
    private SQLiteDatabase mDb;
    private WaitlistDbHelper dbHelper;



    public class MyBinder extends Binder {

        MyServices getService(){

            return MyServices.this;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        dbHelper = new WaitlistDbHelper(this);
        mDb = dbHelper.getWritableDatabase();
        TestUtil.insertFakeData(mDb);
        mAPIService = ApiUtils.getAPIService();
        timer=new Timer();
        task = new TimerTask(){
            @Override
            public void run(){
               /* Intent intent = new Intent(EVENT_ACTION);
                intent.putExtra("value",System.currentTimeMillis());
                MyServices.this.sendBroadcast(intent);*/
                Log.e(TAG,"latitude"+lat1);
                Log.e(TAG,"longitude"+lon1);
                Log.e(TAG,"PASSER"+GetCount());
                Log.e(TAG,"PASSER-PASSER"+ getPrestataireID(GetCount()));

                 long prestId = Long.parseLong(getPrestataireID(GetCount()));
                 UpdatePrestataire( prestId,new Position(lat1,lon1));

                /*Intent i = new Intent();
                i.setClass(getBaseContext(), ConnectionActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);*/

            }
        };
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this,"Started service",Toast.LENGTH_SHORT).show();
        GPStracker g = new GPStracker(getApplicationContext());
        Location l = g.getLocation();
        if(l!=null){
            double lat=l.getAltitude();
            double lon=l.getLongitude();
            lat1=lat;
            lon1=lon;

        }
        if(!started){
            timer.scheduleAtFixedRate(task,0,2000);

            started=true;

        }
        //super.onStartCommand(intent,flags,startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this,"Service Stopped",Toast.LENGTH_SHORT).show();
        timer.cancel();
        super.onDestroy();
    }


    private void UpdatePrestataire(long id,Position position) {


        Call<Prestataire> call = mAPIService.updatePostPrestataire(id,position);
        call.enqueue(new Callback<Prestataire>() {
            @Override
            public void onResponse(Call<Prestataire> call, Response<Prestataire> response) {
                if (response.isSuccessful()) {
                    Prestataire a=response.body();
                    //Toast.makeText(ActivationCompteActivity.this, "" + a.getNom(), Toast.LENGTH_LONG).show();
                }else{
                    String error = null;
                    try {
                        error = response.errorBody().string();
                        JSONObject jsonObjectError = new JSONObject(error);
                        String description = jsonObjectError.getString("description");
                        Toast.makeText(getBaseContext(), ""+description, Toast.LENGTH_SHORT).show();
                        //String errorDetails = jsonObjectError.getString("errorDetails");
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.e(TAG, "TRY"+ response.code());


                }
            }

            @Override
            public void onFailure(Call<Prestataire> call, Throwable t) {


                if(call.isCanceled()) {
                    Toast.makeText(getBaseContext(), "request was aborted", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "request was aborted");
                }else {
                    Toast.makeText(getBaseContext(), "Unable to submit post to API", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Unable to submit post to API.");
                    Log.e(TAG, "doLogin onFaillure: " + t.getMessage());
                }

                Log.e(TAG, "Unable to submit post to API.");


            }

        });


    }

    //   DONNE ID DU DERNIER PRESTATAIRE CONNECTE
    public String getPrestataireID(Integer i)
    {
        String resultat = null;
        Cursor cursor = mDb.rawQuery("SELECT " + WaitlistContract.BasePrestataire.COLUMN_PREST_ID  +" FROM " +TABLE_NAME + " WHERE " + WaitlistContract.BasePrestataire._ID +" = " +i , null);
        if( cursor != null && cursor.moveToFirst() ){
            resultat = cursor.getString(cursor.getColumnIndex(COLUMN_PREST_ID));
            cursor.close();
        }
        // resultat = resultat.intern();
        return resultat;
    }

    //   DONNE LE NOMBRE D'ELEMENT DE LA TABLE
    public int  GetCount()
    {
        Cursor cursor = mDb.rawQuery("SELECT* " +" FROM " +TABLE_NAME , null);


        return cursor.getCount();
    }
}

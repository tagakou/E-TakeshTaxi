package e_takesh.android.e_takeshtaxi.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import e_takesh.android.e_takeshtaxi.R;
import e_takesh.android.e_takeshtaxi.activity.ConnectionActivity;
import e_takesh.android.e_takeshtaxi.activity.PresentationActivity;


/**
 * Created by tagakousoh on 19/03/2018.
 */

public class FragmentPresentation1 extends Fragment {

    private final static String TAG= "FragmentPresentation11";

    RelativeLayout btnSuivant, btnPasser;
    TextView text1,text2;
    RelativeLayout layout;
    ImageView imagedefilement1,imagedefilement2,imagedefilement3;
    LocationManager manager = null;
    @Nullable
    @Override

    // Création du fragment 1

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_presentation1 ,container ,false);
        btnSuivant= view.findViewById(R.id.relativeLayoutSuivant);
        btnPasser= view.findViewById(R.id.relativeLayoutPasser);
        text1=(TextView)view.findViewById(R.id.textEffectuez_deeplacement);
        text2=(TextView)view.findViewById(R.id.textRapidite);
        imagedefilement1=view.findViewById(R.id.image_defilement1);
        imagedefilement2=view.findViewById(R.id.image_defilement2);
        imagedefilement3=view.findViewById(R.id.image_defilement3);

        Drawable d1 = getResources().getDrawable(R.drawable.button_defilement);
        Drawable d2 = getResources().getDrawable(R.drawable.button_defilement_jaune);
        imagedefilement1.setBackground(d2);
        imagedefilement2.setBackground(d1);
        imagedefilement3.setBackground(d1);

        Log.d(TAG,"onCreate: started");
       // text1.setText(getResources().getString(R.string.Presention1));
        //text2.setText(getResources().getString(R.string.Description1));

        // Passer au fragment 2

        btnSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"Go to FragmentPresentation2",Toast.LENGTH_SHORT).show();
                ((PresentationActivity)getActivity()).setViewPager(1);
            }
        });

        // Passer a L'activity connection

        btnPasser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"Go to Fragment1",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ConnectionActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    public static Bitmap decodeImageFile(File f, int WIDTH, int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }
}

package e_takesh.android.e_takeshtaxi.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import e_takesh.android.e_takeshtaxi.R;
import e_takesh.android.e_takeshtaxi.activity.ConnectionActivity;


/**
 * Created by tagakousoh on 19/03/2018.
 */

public class FragmentPresentation3 extends Fragment {

    private final static String TAG= "FragmentPresentation11";

    RelativeLayout btnSuivant, btnPasser;
    TextView text1,text2;
    RelativeLayout layout;
    ImageView imagedefilement1,imagedefilement2,imagedefilement3;
    LocationManager manager = null;
    @Nullable
    @Override

    // Création du fragment 1

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_presentation3 ,container ,false);
        btnSuivant= view.findViewById(R.id.relativeLayoutSuivant);
        btnPasser= view.findViewById(R.id.relativeLayoutPasser);
        text1=(TextView)view.findViewById(R.id.textEffectuez_deeplacement);
        text2=(TextView)view.findViewById(R.id.textRapidite);
        imagedefilement1=view.findViewById(R.id.image_defilement1);
        imagedefilement2=view.findViewById(R.id.image_defilement2);
        imagedefilement3=view.findViewById(R.id.image_defilement3);

        Drawable d1 = getResources().getDrawable(R.drawable.button_defilement);
        Drawable d2 = getResources().getDrawable(R.drawable.button_defilement_jaune);
        imagedefilement1.setBackground(d1);
        imagedefilement2.setBackground(d1);
        imagedefilement3.setBackground(d2);
        Log.d(TAG,"onCreate: started");
       // text1.setText(getResources().getString(R.string.Presention1));
        //text2.setText(getResources().getString(R.string.Description1));

        // Passer au fragment 2

        btnSuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"Go to FragmentPresentation2",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ConnectionActivity.class);
                startActivity(intent);
            }
        });

        // Passer a L'activity connection

        btnPasser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(),"Go to Fragment1",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ConnectionActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }


}

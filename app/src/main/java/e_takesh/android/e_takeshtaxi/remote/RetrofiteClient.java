package e_takesh.android.e_takeshtaxi.remote;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tagakousoh on 23/04/2018.
 */

public class RetrofiteClient {

        private static Retrofit retrofit = null;

        public static Retrofit getClient(String baseUrl) {
            if (retrofit==null) {

                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(400, TimeUnit.SECONDS)
                        .readTimeout(400,TimeUnit.SECONDS)
                        .writeTimeout(400, TimeUnit.SECONDS).build();

                retrofit = new Retrofit.Builder()
                        .client(client)
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit;
        }
    }




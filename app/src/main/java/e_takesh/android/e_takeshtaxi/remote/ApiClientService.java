package e_takesh.android.e_takeshtaxi.remote;
import e_takesh.android.e_takeshtaxi.model.Position;
import e_takesh.android.e_takeshtaxi.model.Prestataire;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by tagakousoh on 23/04/2018.
 */

public interface ApiClientService {

    @GET("provider/prestataire/{paramCone}/login")
    Call<Prestataire> connecterPrestataire(@Path("paramCone") String paramCone);



    @PUT("provider/prestataire/{id}/position")
    Call<Prestataire> updatePostPrestataire(@Path("id") long id,
                                            @Body Position position


    );




  /*  @POST("clients")
    Call<ClientStatus> createAccount(@Body Client client);
    /*Call<Client> savePost(@Field("nom") String nom,
                          @Field("prenom") String prenom,
                          @Field("date_naissance") String date_naissance,
                          @Field("telephone") String telephone,
                          @Field("email") String email,
                          @Field("latitude") Double latitude,
                          @Field("longitude") Double longitude
                         );

    @PUT("posts/{id}")
    @FormUrlEncoded
    Call<Client> updatePost(@Field("id") long id,
                            @Field("nom") String nom,
                            @Field("prenom") String prenom,
                            @Field("date_naissance") String date_naissance,
                            @Field("telephone") String telephone,
                            @Field("email") String email,
                            @Field("latitude") Double latitude,
                            @Field("longitude") Double longitude

    );


    @GET("clients/{id}/activate")
    Call<Client> verifycode(@Path("id") long id, @Query("code") String code);

    @GET("getAllVehicules")
    public Call<List<Vehicule>> getVehicule(@Query("prestataireId") long id);

    @GET("getAllPrestations")
    Call<List<Prestation>> getPrestation(@Query("prestataireId") long id);

    @GET("getAllCustomPrestataires")
    Call<List<Prestataire>> getPrestataireVehiculePrive();

    @GET("getAllCustomPrestataires")
    Call<List<Prestataire>> getPrestataireVehiculeAeoroport();

    @GET("getAllPrestations")
    Call<List<Prestation>> getPrestataireVehiculeService(@Query("prestataireId") long id);

    @GET("getAllPrestations")
    Call<List<Prestation>> getPrestataireService(@Query("prestataireId") long id);

    @GET("getAllCustomPrestataires")
    Call<List<Prestataire>> getPrestataireCustom();


    @GET("clients/resendCode/{telephone}")
    Call<Client> resendCode(@Query("telephone") String telephone);

    @GET("prestataires")
    public Call<List<Prestataire>> listPrestataire();!*/

}

package e_takesh.android.e_takeshtaxi.Utils;


import e_takesh.android.e_takeshtaxi.remote.ApiClientService;
import e_takesh.android.e_takeshtaxi.remote.RetrofiteClient;

/**
 * Created by tagakousoh on 23/04/2018.
 */

public class ApiUtils {

    private ApiUtils() {}
    public static final String BASE_URL = "http://192.168.100.11:8080/etakesh/api/";

    public static ApiClientService getAPIService() {

        return RetrofiteClient.getClient(BASE_URL).create(ApiClientService.class);
    }

}
